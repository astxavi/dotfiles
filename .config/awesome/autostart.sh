#!/usr/bin/env bash 

### AUTOSTART PROGRAMS ###
#lxsession & #session manager
nitrogen --restore & #restore the last wallpaper
#nitrogen --set-zoom-fill --random ~/wallpapers & #randomly set a wallpaper
nm-applet &
picom &
volumeicon &
